var PadManager = require('ep_etherpad-lite/node/db/PadManager'),
    async      = require('ep_etherpad-lite/node_modules/async'),
    settings   = require('ep_etherpad-lite/node/utils/Settings').ep_delete_empty_pads;

var LOG_MESSAGE_TEMPLATE = 'Deleting %s because it is empty';

function readInitHeadRevision() {
    if (typeof settings == 'object' && typeof settings.initial_head_revision == 'number') {
        return settings.initial_head_revision;
    }
    return 0;
}

// Check if we need to delete the pad each time a user leaves
exports.deletePadAtLeave = function(hook, session, cb) {
    if (session !== undefined && session !== null) {
        var pad = session.padId;
        PadManager.doesPadExists(pad, function(err, exists) {
            if (exists !== undefined && exists !== null) {
                if (exists) {
                    PadManager.getPad(pad, function (err, pad) {
                        if (err) {
                            return cb(err);
                        }
                        var head = pad.getHeadRevisionNumber();
                        if (head !== undefined && head !== null) {
                            if (head === readInitHeadRevision()) {
                                console.log(LOG_MESSAGE_TEMPLATE, session.padId);
                                pad.remove(cb);
                            } else {
                                cb();
                            }
                        }
                    });
                } else {
                    cb();
                }
            }
        });
    }
};

// Delete empty pads at startup
exports.deletePadsAtStart = function (hook_name, args, cb) {
    // Deletion queue (avoids max stack size error), 2 workers
    var q = async.queue(function (pad, callback) {
        pad.remove(callback);
    }, 2);
    // Emptyness test queue
    var p = async.queue(function(padId, callback) {
        PadManager.getPad(padId, function (err, pad) {
            if (err) {
                return callback(err);
            }
            var head = pad.getHeadRevisionNumber();
            if (head !== undefined && head !== null) {
                if (head === readInitHeadRevision()) {
                    q.push(pad, function (err) {
                        if (err) {
                            return callback(err);
                        }
                        console.log(LOG_MESSAGE_TEMPLATE, pad.id);
                    });
                }
                callback();
            }
        });
    }, 1);
    PadManager.listAllPads(function (err, data) {
        for (var i = 0; i < data.padIDs.length; i++) {
            var padId = data.padIDs[i];
            p.push(padId, function(err) {
                if (err) {
                    return cb(err);
                }
            });
        }
    });
};
